## 操作说明：

* 构建javaenv

```
mvn clean package docker:build -DpushImage
```

* 为镜像打tag

```bash
docker pull abocode.com:5000/mrray/aichat-javaenv:4.2.2-SNAPSHOT
docker tag  abocode.com:5000/mrray/aichat-javaenv:4.2.2-SNAPSHOT  mrray/aichat-javaenv:4.2.2-SNAPSHOT
docker save -o aichat-javaenv.tar  mrray/aichat-javaenv:4.2.2-SNAPSHOT
docker load -i aichat-javaenv.tar
```

* 环境配置

```
java -Duser.timezone=GMT+08 -jar -Xdebug -Xrunjdwp:transport=dt_socket,suspend=n,server=y,address=5005 raybaas-order-4.1.9-SNAPSHOT.jar

```

* 断点调试

 ```
-Xdebug -Xrunjdwp:transport=dt_socket,suspend=n,server=y,address=5005
```

 