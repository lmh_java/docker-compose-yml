# docker-yml

#### Description
docker-yml docker-compose files 

## Module

```
├─docekr-mongo
├─docker-ceph
├─docker-elk
│  ├─config
│  └─logstash
├─docker-fastdfs
├─docker-ipfs
├─docker-hyperledger
├─docker-java
├─docker-jemeter
├─docker-kafka
├─docker-monitor
│  ├─cadvisor
│  ├─grafana
│  ├─influxdb
│  ├─prometheus
│  └─telegraf
├─docker-mysql
├─docker-nginx
│  ├─conf
│  ├─conf.d
│  └─logs
├─docker-redis
├─docker-zabbix
└─docker-zookeeper
```

## Join Us
* mail：guanxf@aliyun.com
* QQ Group:140586555。